from game_play.console import *
from game_play.game_engine import *
from remembering_guess_checker import *


def run():
    GameEngine.guess_checker = RememberingGuessChecker()
    GameEngine.console = Console()
    engine = GameEngine()
    engine.play()


run()
