from game_play.score import *


def count(string, char):
    c = 0
    for i in range(len(string)):
        if string[i] == char:
            c += 1
    return c


class Console:
    @staticmethod
    def announce_game_over():
        print("Game Over.")

    @staticmethod
    def announce_winning_code(code: str):
        print("Winning code = " + code)

    @staticmethod
    def announce_tries(tries: int):
        print("tries = " + str(tries))

    @staticmethod
    def announce_bad_scoring():
        print("Sorry, but you're scoring was less than perfectly accurate.")

    @staticmethod
    def score_guess(guess):
        print("Enter a score for", guess)
        guess_str = input()
        scr = Score(count(guess_str, "+"), count(guess_str, "-"))
        return scr