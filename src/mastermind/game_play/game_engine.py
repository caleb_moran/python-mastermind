from game_play.console import *
from game_play.guess_checker import *
from game_play.score import *


class Guesser:
    def __init__(self, guess_checker):
        self.guess_checker = guess_checker
        self.guess_index = 0

    def make_guess(self, guess_pos):
        if (guess_pos < 0) or (guess_pos >= GameEngine.MAX_CODES):
            # TODO here is problem
            return None
        else:
            return self.build_guess(guess_pos)

    @staticmethod
    def build_guess(guess_pos):
        n = GameEngine.MAX_LETTERS
        d1 = int(guess_pos % n)
        d2 = int((guess_pos / n) % n)
        d3 = int((guess_pos / (n * n)) % n)
        d4 = int((guess_pos / (n * n * n)) % n)

        return f"{GameEngine.LETTERS[d4]}{GameEngine.LETTERS[d3]}{GameEngine.LETTERS[d2]}{GameEngine.LETTERS[d1]}"

    def get_next_guess(self):
        while True:
            guess = self.make_guess(self.guess_index)
            self.guess_index += 1
            # TODO something is awry, always returns None
            if guess is None:
                return None

            elif self.guess_checker.should_try(guess):
                return guess

    def remember_score(self, guess, score):
        self.guess_checker.add_score(guess, score)


class GameEngine:
    LETTERS = "ABCDEF"
    POSITIONS = 4
    MAX_LETTERS = len(LETTERS)
    MAX_CODES = MAX_LETTERS ** POSITIONS

    def __init__(self):
        self.console = Console()
        self.guess_checker = GuessChecker()
        self.guesser = Guesser(self.guess_checker)
        self.game_over = False
        self.tries = 0

    def play(self):
        self.tries = 1
        while not self.game_over:
            self.try_next_guess(self.guesser.get_next_guess())
            self.tries += 1

    def try_next_guess(self, guess):
        if guess is None:
            self.code_not_found()
        else:
            self.score_one_guess(guess)

    def score_one_guess(self, guess):
        score = self.console.score_guess(guess)
        if self.is_perfect_match(score):
            self.win(guess)
        else:
            self.guesser.remember_score(guess, score)

    def win(self, guess):
        self.console.announce_game_over()
        self.console.announce_winning_code(guess)
        self.console.announce_tries(self.tries)
        self.game_over = True

    def is_perfect_match(self, score):
        return score.in_position() == 4

    def code_not_found(self):
        self.console.announce_game_over()
        self.console.announce_tries(self.tries - 1)
        self.console.announce_bad_scoring()
        self.game_over = True
