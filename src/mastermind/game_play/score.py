class Score:
    def __init__(self, in_position, in_code):
        self.in_position = in_position
        self.in_code = in_code

    def __eq__(self, other):
        if isinstance(other, Score):
            score = other
            return score.in_position == self.in_position and score.in_code == self.in_code

        return False

    def __str__(self):
        return f"({self.in_position}, {self.in_code}"

    def in_position(self):
        return self.in_position

    def in_code(self):
        return self.in_code
