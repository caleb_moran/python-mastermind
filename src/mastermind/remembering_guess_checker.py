from game_play.guess_checker import *
from scorer import Scorer


class ScoreRecord:
    def __init__(self, guess: str, score: str):
        self.guess = guess
        self.score = score


class RememberingGuessChecker(GuessChecker):
    def __init__(self):
        self.score_history = []

    def should_try(self, guess):
        return self.is_guess_consistent_with_history(guess)

    def add_score(self, guess, score):
        self.score_history.append(ScoreRecord(guess, score))

    def is_guess_consistent_with_history(self, guess):
        assumed_code = guess
        for i in range(len(self.score_history)):
            if (
                Scorer.score_guess(assumed_code, self.score_history[i].guess)
                != self.score_history[i].score
            ):
                return False

        return True
