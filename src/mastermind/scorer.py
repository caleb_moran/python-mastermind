from game_play.score import *


class Scorer:
    def __init__(self, code: str, guess: str):
        self.code = code
        self.guess = guess
        self.code_position_used = [None] * 4  # boolean list, size of 4
        self.guess_position_used = [None] * 4  # boolean list, size of 4

    @staticmethod
    def score_guess(code, guess):
        return Scorer(code, guess).score_it()

    def score_it(self):
        return Score(self.count_letters_in_pos(), self.count_letters_out_pos())

    def is_in_position(self, i):
        if self.code[i] == self.guess[i]:
            self.code_position_used[i] = True
            self.guess_position_used[i] = True
            return True
        else:
            return False

    def is_out_position(self, index):
        for loop_pos in range(len(self.guess)):
            if (
                not self.code_position_used[index]
                and not self.guess_position_used[loop_pos]
                and index != loop_pos
                and self.guess[loop_pos] == self.code[index]
            ):
                self.code_position_used[index] = True
                self.guess_position_used[loop_pos] = True
                return True

        return False

    def count_letters_in_pos(self):
        in_pos = 0
        for i in range(len(self.code)):
            if self.is_in_position(i):
                in_pos += 1

        return in_pos

    def count_letters_out_pos(self):
        out_pos = 0
        for i in range(len(self.code)):
            if self.is_out_position(i):
                out_pos += 1

        return out_pos
