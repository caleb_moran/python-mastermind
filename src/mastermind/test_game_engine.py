from unittest import TestCase

from game_play.console import *
from game_play.guess_checker import *
from game_play.score import *
from game_play.game_engine import *
from game import *
from remembering_guess_checker import *
from scorer import *


class GameEngineTest(TestCase):
    def setUp(self):
        self.checker = MockGuessChecker()
        self.console = MockConsole()
        GameEngine.guess_checker = self.checker

    def test_got_it_on_first_guess(self):
        self.run_game(FirstTryMockConsole())
        # self.assertTrue(self.console.announced_game_over())  # <-- issue
        self.assertEqual("AAAA", self.console.get_winning_code())
        self.assertEqual(1, self.console.get_tries())
        self.assertEqual(0, self.checker.guesses_added())

    def test_got_it_on_second_try(self):
        self.run_game(SecondTryMockConsole())
        self.assertTrue(self.console.announced_game_over())
        self.assertEqual("AAAB", self.console.get_winning_code())
        self.assertEqual(2, self.console.get_tries())
        self.assertEqual(1, self.checker.guesses_added())

    def test_never_get_it(self):
        self.run_game(NoTriesMockConsole())
        self.assertTrue(self.console.announced_game_over())
        self.assertEqual(GameEngine.MAX_CODES, self.console.get_tries())
        self.assertTrue(self.console.announced_bad_scoring())
        self.assertEqual(GameEngine.MAX_CODES, self.checker.guessesAdded())

    def run_game(self, mock_console):
        self.console = mock_console
        GameEngine.console = self.console
        engine = GameEngine()
        engine.play()


class MockConsole(Console):
    def __init__(self):
        self.game_over = False
        self.winning_code = None
        self.tries = 0
        self.bad_scoring = False

    @staticmethod
    def score_guess(guess):
        return None

    def announce_game_over(self):
        self.game_over = True

    def announce_winning_code(self, code):
        self.winning_code = code

    def announce_tries(self, tries):
        self.tries = tries

    def announce_bad_scoring(self):
        self.bad_scoring = True

    def announced_game_over(self):
        return self.game_over

    def get_winning_code(self):
        return self.winning_code

    def get_tries(self):
        return self.tries

    def announced_bad_scoring(self):
        return self.bad_scoring


class FirstTryMockConsole(MockConsole):
    @staticmethod
    def score_guess(guess):
        return Score(4, 0)


class SecondTryMockConsole(MockConsole):
    def __init__(self):
        self.guessed_yet = False

    def score_guess(self, guess):
        if self.guessed_yet:
            return Score(4, 0)
        else:
            self.guessed_yet = True
            return Score(0, 0)


class NoTriesMockConsole(MockConsole):
    @staticmethod
    def score_guess(guess):
        return Score(0, 0)


class MockGuessChecker(GuessChecker):
    def __init__(self):
        self.guesses_added = 0

    @staticmethod
    def should_try(guess):
        return True

    def add_score(self, guess, score):
        self.guesses_added += 1

    def guesses_added(self):
        return self.guesses_added
