from unittest import TestCase

from game_play.console import *
from game_play.guess_checker import *
from game_play.score import *
from game_play.game_engine import *
from game import *
from remembering_guess_checker import *
from scorer import *


class GuesserTest(TestCase):
    def setUp(self):
        self.failing_checker = GuessChecker()

        @staticmethod
        def should_try(guess):
            return False

        def add_score(guess, score):
            pass

    def test_when_no_guesses_are_valid_next_guess_is_nil(self):
        self.guesser = Guesser(self.failing_checker)
        self.assertIsNone(self.guesser.get_next_guess())

    def test_when_only_one_guess_is_valid_next_guess_is_that_one(self):
        self.guesser = Guesser(SingleChecker("BEEF"))
        self.assertEqual("BEEF", self.guesser.get_next_guess())
        self.assertIsNone(self.guesser.get_next_guess())

    def test_make_guess(self):
        self.assertEqual("AAAA", Guesser.make_guess(0))
        self.assertEqual("AAAB", Guesser.make_guess(1))
        self.assertEqual(
            "FFFF", Guesser.make_guess(GameEngine.MAX_CODES - 1)
        )
        self.assertIsNone(Guesser.make_guess(GameEngine.MAX_CODES))
        self.assertIsNone(Guesser.make_guess(-1))


class SingleChecker(GuessChecker):
    def __init__(self, good_guess):
        self.valid_guess = good_guess

    def should_try(self, guess):
        return self.valid_guess == guess

    def add_score(self, guess: str, score: Score):
        pass
