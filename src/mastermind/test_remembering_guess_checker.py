from unittest import TestCase

from game_play.console import *
from game_play.guess_checker import *
from game_play.score import *
from game_play.game_engine import *
from game import *
from remembering_guess_checker import *
from scorer import *


class RememberingGuessCheckerTest(TestCase):
    def setUp(self):
        self.checker = RememberingGuessChecker()

    def test_with_no_guesses_every_guess_passes(self):
        for i in range(GameEngine.MAX_CODES):
            self.assertTrue(self.checker.should_try(Guesser.make_guess(i)))

    def test_any_guess_that_does_not_match_will_fail_again(self):
        self.checker.add_score("AAAA", Score(0, 0))
        self.checker.add_score("BBBB", Score(1, 1))
        self.test_assertGuesses("~AAAA", "~BBBB")

    def test_if_there_are_no_As_only_guesses_with_As_will_fail(self):
        self.checker.add_score("AAAA", Score(0, 0))
        self.test_assertGuesses("~ABCD", "~BBAB", "~CCCA", "BBBB")

    def test_if_there_are_no_As_or_Bs_only_guesses_with_As_and_Bs_will_fail(self):
        self.checker.add_score("AAAA", Score(0, 0))
        self.checker.add_score("BBBB", Score(0, 0))
        self.test_assertGuesses("~CCCA", "~CCCB", "CCCD")

    def test_if_theres_one_in_the_right_pos_then_only_those_that_have_just_one_in_pos_will_pass(
        self,
    ):
        self.checker.add_score("ABCD", Score(1, 0))
        self.test_assertGuesses(
            "AEEE", "EBEE", "EECE", "EEED", "~DCBA", "~EEEE", "~ABCD", "~ABEE"
        )

    def test_if_theres_one_out_of_pos_then_only_those_that_have_just_one_in_another_pos_will_pass(
        self,
    ):
        self.checker.add_score("ABCD", Score(0, 1))
        self.test_assertGuesses(
            "EAEE", "BEEE", "ECEE", "DEEE", "EEEA", "~AEEE", "~EECE", "~EEAB"
        )

    def test_there_is_one_A(self):
        self.checker.add_score("AAAA", Score(1, 0))
        self.test_assertGuesses("ABBB")

    def test_assertGuesses(self, *guesses):
        for guess in guesses:
            if guess.startswith("~"):
                self.my_guess = guess[1:]
                self.assertFalse(guess, self.checker.should_try(self.my_guess))
            else:
                self.assertTrue(guess, self.checker.should_try(guess))
