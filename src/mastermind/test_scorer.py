from unittest import TestCase

from game_play.console import *
from game_play.guess_checker import *
from game_play.score import *
from game_play.game_engine import *
from game import *
from remembering_guess_checker import *
from scorer import *


class ScorerTest(TestCase):
    @staticmethod
    def score(in_position, in_code):
        return Score(in_position, in_code)

    def test_in_position(self):
        self.assertInPosition(0, "ABCD", "DCBA")
        self.assertInPosition(1, "ABBB", "ACCC")
        self.assertInPosition(2, "AABB", "AACC")
        self.assertInPosition(2, "ABAB", "ACAC")
        self.assertInPosition(3, "DEEE", "AEEE")
        self.assertInPosition(3, "BABB", "BCBB")
        self.assertInPosition(4, "AAAA", "AAAA")
        self.assertInPosition(1, "ABBB", "AAAA")

    def test_not_in_position(self):
        self.assertNotInPosition(0, "AAAA", "BBBB")
        self.assertNotInPosition(0, "ABBB", "ACCC")
        self.assertNotInPosition(1, "ABBB", "CACC")
        self.assertNotInPosition(4, "ABCD", "DCBA")
        self.assertNotInPosition(2, "ABBA", "CAAC")
        self.assertNotInPosition(3, "ABCD", "CADF")
        self.assertNotInPosition(0, "ABBB", "AAAA")
        self.assertNotInPosition(1, "ABBB", "CAAA")
        self.assertNotInPosition(1, "AABB", "CCAC")

    def test_with_no_matches_score_is_None(self):
        self.assertInPosition(0, "AAAA", "BBBB")

    def assertInPosition(self, in_pos, code, guess):
        s = Scorer.score_guess(code, guess)
        print(type(s))
        self.assertEqual(in_pos, s.in_position())
    
    def assertNotInPosition(self, in_pos, code, guess):
        self.assertEqual(in_pos, Scorer.score_guess(code, guess).in_code())
        

